<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function form(){

        return view('halaman.biodata');
    }
    public function selesai(Request $request){
        $depan = $request->fname;
        $belakang = $request->lname;

        return view('halaman.selesai', compact('depan','belakang'));
    }
}
