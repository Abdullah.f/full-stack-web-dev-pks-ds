<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Film;
use File;
use RealRashid\SweetAlert\Facades\Alert;

class FilmController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except('index','show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $film = Film::all();

        return view('film.index', compact('film'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = DB::table('genre')->get();
        return view('film.create',compact('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'tahun' => 'required',
            'ringkasan' => 'required',
            'genre_id' => 'required',
            'poster' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $postername = time().'.'.$request->poster->extension(); 

        $request->poster->move(public_path('filmimage'), $postername);

        $film = new Film;

        $film->judul = $request->judul;
        $film->tahun = $request->tahun;
        $film->ringkasan = $request->ringkasan;
        $film->genre_id = $request->genre_id;
        $film->poster = $postername;

        $film->save();
        Alert::success('Success', 'Data Berhasil Ditambahkan!');

        return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::findOrfail($id);

        return view('film.show', compact('film'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $genre = DB::table('genre')->get();

        $film = Film::findOrfail($id);

        return view('film.edit', compact('film','genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'tahun' => 'required',
            'ringkasan' => 'required',
            'genre_id' => 'required',
            'poster' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $film = Film::find($id);

        if($request->has('poster')){
            $postername = time().'.'.$request->poster->extension(); 

            $request->poster->move(public_path('filmimage'), $postername);
    
            $film->judul = $request->judul;
            $film->tahun = $request->tahun;
            $film->ringkasan = $request->ringkasan;
            $film->genre_id = $request->genre_id;
            $film->poster = $postername;        
        }else{
            $film->judul = $request->judul;
            $film->tahun = $request->tahun;
            $film->ringkasan = $request->ringkasan;
            $film->genre_id = $request->genre_id;
        }

        $film->update();
        Alert::success('Success', 'Data Berhasil Diupdate!');

        return redirect('/film');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = Film::find($id);

        $path = "filmimage/";
        File::delete($path . $film->poster);
        $film->delete();
        Alert::success('Success', 'Data Berhasil Dihapus!');

        return redirect('/film');
    }
}
