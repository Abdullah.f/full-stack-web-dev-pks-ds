<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/film" class="brand-link">
      <img src="{{asset('admin/dist/img/R.png')}}" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Review Film</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('admin/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        @auth
        <div class="info">
          <a href="/login" class="d-block">{{Auth::user()->name}}</a>
        </div> 
        @endauth
        @guest
         <div class="info">
          <a href="/login" class="d-block">Halaman Guest</a>
        </div>   
        @endguest
        
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        @auth
        <li class="nav-item">
          <a href="/film" class="nav-link">
            <i class="nav-icon fa fa-film"></i>
            <p>
            Film
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="/genre" class="nav-link">
            <i class="nav-icon fa fa-th-list"></i>
            <p>
            Genre Film
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="/cast" class="nav-link">
            <i class="nav-icon fa fa-th-list"></i>
            <p>
            Cast List
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link bg-danger" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
            <i class="fa fa-sign-out" aria-hidden="true"></i>
            <p>Logout</p>
        </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
          </form>
        </li>
     @endauth

     @guest
     <li class="nav-item">
      <a href="/login" class="nav-link bg-primary">
        <p>
        Login
        </p>
      </a>
   </li>  
     @endguest
    
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>