@extends('layout.master')

@section('judul')
	Halaman Detail cast Film {{$cast->nama}}
@endsection

@section('content')

<h3>{{$cast->nama}} <br><br></h3>
<h3>{{$cast->umur}} <br><br></h3>
<h3>{{$cast->bio}} <br><br></h3>
<a href="/cast/" class="btn btn-info mt-3 ">Kembali</a>

@endsection