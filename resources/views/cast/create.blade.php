@extends('layout.master')

@section('judul')
	Halaman Tambah Caster
@endsection

@push('script')
  <script src="https://cdn.tiny.cloud/1/7qnnlkuknh3kgb3hgc79492u34cckewpjkvernwalfzwt6xo/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
  <script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
@endpush

@section('content')
 
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name='nama' class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="number" name='umur' class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label>Bio</label>
      <textarea name="bio" class="form-control" cols="30" rows="10"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
    
</form>


@endsection