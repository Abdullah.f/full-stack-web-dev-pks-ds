@extends('layout.master')

@section('judul')
	Halaman List genre
@endsection

@section('content')

<a href="/genre/create" class="btn btn-info mb-3">Tambah Data</a>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama Genre</th>
        <th scope="col">List Film</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($genre as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>
                    <ul>
                        @foreach ($item->film as $value)
                            <li>{{$value->judul}}</li>
                        @endforeach
                     </ul>
                        
                </td>

                <td>
                    
                    <form action="/genre/{{$item->id}}" method="POST">
                        @method('delete')
                        @csrf
                            <a href="/genre/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/genre/{{$item->id}}/edit" class="btn btn-success btn-sm">Edit</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td>Data Masih Kosong</td>
            </tr>
        @endforelse
    </tbody>
</table> 

  @endsection