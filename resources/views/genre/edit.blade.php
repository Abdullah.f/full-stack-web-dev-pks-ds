@extends('layout.master')

@section('judul')
	Halaman Edit Genre {{$genre->nama}}
@endsection

@section('content')
 
<form action="/genre/{{$genre->id}}" method="POST">
    @csrf
    @method("PUT")
    <div class="form-group">
      <label>Genre Film</label>
      <input type="text" name="nama" value="{{$genre->nama}}" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection