<?php

trait Hewan {

    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function infoHewan()
    {
    echo "Ini adalah hewan " . $this->nama .
    " dengan total darah " . $this->darah .
    ", jumlah kaki " . $this->jumlahKaki .
    ", dan keahlian " . $this->keahlian .
    ".";
    }

    public function atraksi()
    {
    echo $this->nama . " sedang " .
    $this->keahlian . ".";
    }

}

trait Fight {
    
    use Hewan;
    public $attackPower;
    public $defensePower;

    public function infoStat()
    {
    echo "Hewan ini memiliki Attak Power sebesar : " . $this->attackPower .
    " dan Defense Power sebesar : " . $this->defensePower .
    ".";
    }

    public function serang()
    {
    echo $this->nama . " sedang menyerang " . 
    $this->nama .
    ".";
    echo "<br>";
    $this->diserang();
    }

    public function diserang()
    {
    echo $this->nama . " sedang diserang " . 
    $this->nama . " dan mengurangi darahnya menjadi " .
    $this->darah - ($this->attackPower - $this->defensePower) .
    ".";
    }

}

class Elang {

    use Hewan;
    use Fight;

    public function getInfoHewan()
    {

    $this->nama= "Elang";
    $this->jumlahKaki= 2;
    $this->keahlian= "terbang tinggi";

    $this->attackPower= 10;
    $this->defensePower= 5;

    echo $this->infoHewan();
    echo "<br>";
    echo $this->infoStat();
    echo "<br>";

    }

}

class Harimau {

    use Hewan;
    use Fight;

    public function getInfoHewan()
    {
    
    $this->nama= "Harimau";
    $this->jumlahKaki= 4;
    $this->keahlian= "lari cepat";

    $this->attackPower= 7;
    $this->defensePower= 8;

    echo $this->infoHewan();
    echo "<br>";
    echo $this->infoStat();
    echo "<br>";

    }

}

class Hewan1 extends Elang {
    
    public function aksi1()
    {
        echo $this->atraksi();
        echo "<br>";
        echo $this->serang();
        echo "<br>";
    }

}

class Hewan2 extends Harimau {
    
    public function aksi2()
    {
        echo $this->atraksi();
        echo "<br>";
        echo $this->serang();
        echo "<br>";
    }

}


$elang_1 = new Hewan1();
$elang_1->getInfoHewan();
echo "<br><br>";

$harimau_3 = new Hewan2();
$harimau_3->getInfoHewan();
echo "<br><br>";

$elang_1->aksi1();
echo "<br><br>";

$harimau_3->aksi2();
echo "<br><br>";

?>