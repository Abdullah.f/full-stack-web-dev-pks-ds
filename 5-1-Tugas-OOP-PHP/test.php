<?php

class Buah {

    public $nama;
    public $berat;

    public function __construct($nama , $berat)
    {
    $this->nama = $nama;
    $this->berat = $berat;
    }

    function tampilkan()
    {
    return "Buah ini adalah buah " . $this->nama .
            " dengan berat " . $this->berat .
            " gram.";
    }

}

class Hewan {

    public $jumlah_kaki = 4;

    public function __destruct()
    {
    echo'Hewan dihapus dari memory';
    }

}

$mangga = new Buah("mangga" , 300);
echo $mangga->tampilkan();
echo "<br>";
echo "<br>";

$kucing = new Hewan();
echo "Jumlah kaki hewan ini adalah " . $kucing->jumlah_kaki;
echo "<br>";
unset($kucing);

?>