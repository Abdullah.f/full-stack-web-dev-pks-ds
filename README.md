## **Final Project**

## Kelompok 27

## Anggota Kelompok

-   Debby Bayu Kuncoro
-   Nabil Taqiuddin Madjid
-   Abdullah Fahminuddin

## Tema Project

Aplikasi Review Film

## ERD

<p align="center"><a href="#"><img src="public/admin/erd/ERD.png" width="100%"></a></p>

## Deskripsi

-   Template : Menggunakan template dari AdminLTE-3.10
-   Demo : Demo yang ditunjukkan pada video berupa proses authentification (registrasi/login), crud (manipulasi data film), dan menampilkan fitur eloquent dengan menambahkan komentar menggunakan akun teregistrasi pada postingan beberapa film.

## Link Video

-   Link Demo Aplikasi : https://youtu.be/sZWaVZTua_I
-   Link Deploy(optional) : https://final-project-kel27.herokuapp.com
