<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index'); 



Route::post('/selesai','FormController@selesai');

Route::get('/data-table', function(){
    return view('table.data-table');
});

Route::group(['middleware' => ['auth']], function () {
    // CRUD genre
Route::get('/genre/create','GenreController@create');
Route::post('/genre','GenreController@store');
Route::get('/genre','GenreController@index');
Route::get('/genre/{genre_id}','GenreController@show');
Route::get('genre/{genre_id}/edit','GenreController@edit');
Route::put('/genre/{genre_id}','GenreController@update');
Route::delete('/genre/{genre_id}','GenreController@destroy');

// Update Profile
Route::resource('profil','ProfileController')->only([
    'index', 'update'
]);
// Kritik
Route::resource('kritik','KritikController')->only([
    'index', 'store'
]);

});



// CRUD cast
Route::get('/cast/create','CastController@create');
Route::post('/cast','CastController@store');
Route::get('/cast','CastController@index');
Route::get('/cast/{cast_id}','CastController@show');
Route::get('cast/{cast_id}/edit','CastController@edit');
Route::put('/cast/{cast_id}','CastController@update');
Route::delete('/cast/{cast_id}','CastController@destroy');

// CRUD film
Route::get('/film/create','FilmController@create');
Route::post('/film','FilmController@store');
Route::get('/film','FilmController@index');
Route::get('/film/{film_id}','FilmController@show');
Route::get('film/{film_id}/edit','FilmController@edit');
Route::put('/film/{film_id}','FilmController@update');
Route::delete('/film/{film_id}','FilmController@destroy');



Auth::routes();


